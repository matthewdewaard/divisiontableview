//
//  DivisionTableHeaderView.swift
//  DivisionTableView-iOS
//
//  Created by Matthew de Waard on 24/06/20.
//  Copyright © 2020 Celo. All rights reserved.
//

import UIKit

class DivisionTableHeaderView: UITableViewHeaderFooterView {
    
    enum Header {
        case division
        case section
    }
    
    @IBOutlet weak var divisionContainerTopConstraint: NSLayoutConstraint?
    @IBOutlet weak var sectionContainerTopConstraint: NSLayoutConstraint?
    @IBOutlet weak var divisionContainerView: UIView!
    @IBOutlet weak var sectionContainerView: UIView!
    
    func setup(_ divisionView: UIView?, _ sectionView: UIView?) {
        insert(divisionView, into: divisionContainerView)
        insert(sectionView, into: sectionContainerView)
        layoutIfNeeded()
    }
    
    fileprivate func insert(_ view: UIView?, into parent: UIView) {
        parent.subviews.forEach { $0.removeFromSuperview()}
        guard let view = view else { return }
        view.translatesAutoresizingMaskIntoConstraints = false
        parent.addSubview(view)
        parent.addConstraints([view.heightAnchor.constraint(equalTo: parent.heightAnchor),
                               view.leadingAnchor.constraint(equalTo: parent.leadingAnchor),
                               view.widthAnchor.constraint(equalTo: parent.widthAnchor),
                               view.topAnchor.constraint(equalTo: parent.topAnchor)])
    }
    
    func didEndScroll() {
        divisionContainerTopConstraint?.constant = 0
        sectionContainerTopConstraint?.constant = 0
    }
    
    func didScrollDivision(offsetInHeader offset: CGFloat) {
        divisionContainerTopConstraint?.constant = -offset
    }
    
    /// Returns false if the offset will pan the container view out of sight
    func didScrollIntoSection(offsetInHeader offset: CGFloat) -> Bool {
        if offset < 0 || offset >= sectionContainerView.frame.height {
            return false
        }
        sectionContainerTopConstraint?.constant = -offset
        return true
    }
}
