//
//  DivisionSectionHeaderView.swift
//  DivisionTableView-iOS
//
//  Created by Matthew de Waard on 16/07/20.
//  Copyright © 2020 Celo. All rights reserved.
//

import UIKit

class DivisionSectionHeaderView: UITableViewHeaderFooterView {

    static let reuseIdentifier: String = String(describing: DivisionSectionHeaderView.self)
    
    lazy var _label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 14.0)
        self.addSubview(label)
        self.addConstraints([
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.0),
            label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.0),
            label.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8.0),
            label.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0),
        ])
        return label
    }()
    
    override var textLabel: UILabel? {
        return _label
    }


}
