//
//  DivisionTableView.swift
//  Celo
//
//  Created by Matthew de Waard on 24/06/20.
//  Copyright © 2020 Celo. All rights reserved.
//

import Foundation
import UIKit

public struct DivisionIndexPath {
    public var division: Int
    public var section: Int
    public var row: Int
}

public protocol DivisionTableViewDataSource: class {
    
    func numberOfDivisions(in tableView: UITableView) -> Int
    func divisionTableView(_ tableView: UITableView, numberOfSectionsInDivision division: Int) -> Int
    func divisionTableView(_ tableView: UITableView, numberOfRowsInSection section: Int, division: Int) -> Int
    func divisionTableView(_ tableView: UITableView, cellForRowAt indexPath: DivisionIndexPath) -> UITableViewCell
    
    func divisionTableView(_ tableView: UITableView, titleForHeaderInDivision division: Int) -> String?
    func divisionTableView(_ tableView: UITableView, titleForHeaderInSection section: Int, division: Int) -> String?
    
    func divisionTableView(_ tableView: UITableView, viewForHeaderInDivision division: Int) -> UIView?
    func divisionTableView(_ tableView: UITableView, viewForHeaderInSection section: Int, division: Int) -> UIView?
}

public protocol DivisionTableViewDelegate: class {
    func divisionTableView(_ tableView: UITableView, didSelectRowAt indexPath: DivisionIndexPath)
}

public extension DivisionTableViewDataSource {
    func divisionTableView(_ tableView: UITableView, titleForHeaderInDivision division: Int) -> String? {
        return nil
    }
    func divisionTableView(_ tableView: UITableView, titleForHeaderInSection section: Int, division: Int) -> String? {
        return nil
    }
    func divisionTableView(_ tableView: UITableView, viewForHeaderInDivision division: Int) -> UIView? {
        guard let title = self.divisionTableView(tableView, titleForHeaderInDivision: division) else {
            return nil
        }
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: DivisionSectionHeaderView.reuseIdentifier)
        view?.textLabel?.text = title
        return view
    }
    
    func divisionTableView(_ tableView: UITableView, viewForHeaderInSection section: Int, division: Int) -> UIView? {
        guard let title = self.divisionTableView(tableView, titleForHeaderInSection: section, division: division) else {
            return nil
        }
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: DivisionSectionHeaderView.reuseIdentifier)
        view?.textLabel?.text = title
        return view
    }
}


/*
 This class defines a custom UITableView with additional grouping of sections.
 For example, Division A can have sections [1, 2, 3], with rows [a, b, c]
 */

/*
 As a user, I want to:
 - define my divisions (title, number of sections)
 - define my sections (title, number of rows in section)
 - define my cells (custom UI for a given row in a section)
 */
open class DivisionTableView: UIView {
    
    // MARK: - Private properties
    fileprivate var _lastContentOffset: CGFloat?
    fileprivate var _numberOfDivisions: Int?
    fileprivate var _numberOfSectionsInDivision: [Int : Int] = [:]
    fileprivate var _numberOfRowsInSection: [String : Int] = [:]
    fileprivate var _floatingHeaderSectionIndex: Int = 0
    
    // This view simulates the behaviour of the floating header from a plain styled tableview
    fileprivate lazy var floatingHeader: DivisionTableHeaderView = {
        let nib = UINib(nibName: headerIdentifier, bundle: Bundle(for: DivisionTableHeaderView.self))
        let view = nib.instantiate(withOwner: nil, options: nil).first as! DivisionTableHeaderView
        view.translatesAutoresizingMaskIntoConstraints = false
        view.accessibilityIdentifier = "DivisionTableView.FloatingHeader"
        return view
    }()
    
    fileprivate lazy var tableView: UITableView = {
        let tableView = UITableView(frame: self.frame, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    
    // MARK: - Public properties
    
    open weak var delegate: DivisionTableViewDelegate?
    open weak var dataSource: DivisionTableViewDataSource? {
        didSet {
            updateFloatingHeader()
        }
    }
    
    
    
    // MARK: - Initial setup
    
    open override var backgroundColor: UIColor? {
        didSet {
            tableView.backgroundColor = backgroundColor
        }
    }
    
    open override func draw(_ rect: CGRect) {
        super.draw(rect)
        // set tableview background here as a workaround.
        // Tableview background will default to grouped colour during init
        tableView.backgroundColor = backgroundColor
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialSetup()
    }
    
    fileprivate func initialSetup() {
        let nib = UINib(nibName: headerIdentifier, bundle: Bundle(for: DivisionTableHeaderView.self))
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: headerIdentifier)
        tableView.register(DivisionSectionHeaderView.self, forHeaderFooterViewReuseIdentifier: DivisionSectionHeaderView.reuseIdentifier)
        tableView.estimatedSectionHeaderHeight = 20
        embedSubviews()
    }
    
    fileprivate func embedSubviews() {
        let header = self.floatingHeader
        let tableView = self.tableView
        addSubview(tableView)
        addSubview(header)
        
        addConstraints([tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
                        tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
                        tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
                        tableView.topAnchor.constraint(equalTo: topAnchor),
                        header.trailingAnchor.constraint(equalTo: trailingAnchor),
                        header.leadingAnchor.constraint(equalTo: leadingAnchor),
                        header.topAnchor.constraint(equalTo: topAnchor)])
    }
    
    fileprivate func updateFloatingHeader() {
        setup(header: floatingHeader, forSection: 0)
        floatingHeader.layoutIfNeeded()
        tableView.contentInset.top = floatingHeader.frame.height
        tableView.scrollIndicatorInsets = tableView.contentInset
    }
    
    // Reload table
    open func reloadData() {
        
        _numberOfDivisions = nil
        _numberOfSectionsInDivision = [:]
        _numberOfRowsInSection = [:]
        _floatingHeaderSectionIndex = -1
        
        let originalContentOffset = tableView.contentOffset
        tableView.reloadData()
        tableView.layoutIfNeeded()
        tableView.contentOffset = originalContentOffset
        
        _adjustForScrollOffset(tableView)
    }
    
    
    
    
    // MARK: - Register custom classes and nibs
    
    open func register(_ cellClass: AnyClass?, forCellReuseIdentifier identifier: String) {
        tableView.register(cellClass, forCellReuseIdentifier: identifier)
    }
    
    open func register(_ nib: UINib?, forCellReuseIdentifier identifier: String) {
        tableView.register(nib, forCellReuseIdentifier: identifier)
    }
    
    open func register(_ cellClass: AnyClass?, forHeaderFooterViewReuseIdentifier identifier: String) {
        tableView.register(cellClass, forHeaderFooterViewReuseIdentifier: identifier)
    }
    
    open func register(_ nib: UINib?, forHeaderFooterViewReuseIdentifier identifier: String) {
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: identifier)
    }
    
    
    // MARK: - Section mapping
    
    /// Total number of divisions
    fileprivate var numberOfDivisions: Int {
        //        if let numberOfDivisions = _numberOfDivisions { return numberOfDivisions }
        let n = dataSource?.numberOfDivisions(in: self.tableView) ?? 0
        _numberOfDivisions = n
        return n
    }
    
    /// Total number of sections within a division
    fileprivate func numberOfSections(in division: Int) -> Int {
        //        if let numberOfSections = _numberOfSectionsInDivision[division] { return numberOfSections }
        let n = dataSource?.divisionTableView(self.tableView, numberOfSectionsInDivision: division) ?? 0
        _numberOfSectionsInDivision[division] = n
        return n
    }
    
    // Total number of rows in section
    fileprivate func numberOfRows(in division: Int, section: Int) -> Int {
        let key = "\(division)-\(section)"
        //        if let numberOfRows = _numberOfRowsInSection[key] { return numberOfRows }
        let n = dataSource?.divisionTableView(self.tableView, numberOfRowsInSection: section, division: division) ?? 0
        _numberOfRowsInSection[key] = n
        return n
    }
    
    /// Maps a standard iOS table section to a division and section index
    fileprivate func mapSectionIndex(at section: Int) -> (divider: Int, section: Int)  {
        var dividerIndex = 0  // The index of the section within the division
        var index = 0         // The section index for the native tableView
        
        while index + numberOfSections(in: dividerIndex) <= section {
            index += numberOfSections(in: dividerIndex)
            dividerIndex += 1
        }
        let sectionIndex = section - index
        return (dividerIndex, sectionIndex)
    }
    
    fileprivate func isFirstSectionInDivision(section: Int) -> Bool {
        let (_, sectionIndex) = mapSectionIndex(at: section)
        return sectionIndex == 0
    }
    
    fileprivate func divisionIndexPath(for path: IndexPath) -> DivisionIndexPath {
        let (division, section) = mapSectionIndex(at: path.section)
        return DivisionIndexPath(division: division, section: section, row: path.row)
    }
}


// MARK: - Public extensions

extension DivisionTableView: UITableViewDataSource {
    
    public final func numberOfSections(in tableView: UITableView) -> Int {
        return (0..<numberOfDivisions).reduce(0, {$0 + numberOfSections(in: $1)})
    }
    
    public final func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let (divisionIndex, sectionIndex) = mapSectionIndex(at: section)
        return numberOfRows(in: divisionIndex, section: sectionIndex)
    }
    
    public final func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = dataSource?.divisionTableView(tableView, cellForRowAt: divisionIndexPath(for: indexPath))
        return cell ?? UITableViewCell()
    }
}

extension DivisionTableView: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.divisionTableView(tableView, didSelectRowAt: divisionIndexPath(for: indexPath))
    }
    
    public final func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    public final func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // The table does not show the first header
        guard section > 0 else { return nil }
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as? DivisionTableHeaderView
        setup(header: view, forSection: section)
        return view
    }
    
}


extension DivisionTableView {
    
    public final func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == tableView else { return }
        _adjustForScrollOffset(scrollView)
    }
    
    fileprivate func _adjustForScrollOffset(_ scrollView: UIScrollView) {
        
        var didAdjustHeader: Bool = false
        var currentSection: Int = 0
        
        guard numberOfDivisions > 0 else {
            floatingHeader.isHidden = true
            return
        }
        
        floatingHeader.isHidden = false
        
        defer {
            // If we havent adjusted the header, setup the header for the current section
            if !didAdjustHeader {
                floatingHeader.didEndScroll()
                if _floatingHeaderSectionIndex != currentSection {
                    setup(header: floatingHeader, forSection: currentSection, forceDivision: true)
                    _floatingHeaderSectionIndex = currentSection
                }
            }
        }
        
        // The header will only ever adjust when there are two or more sections to display
        guard tableView.numberOfSections > 1 else {
            return
        }
        
        let floatingHeaderRect = rectForFloatingHeader
        
        // Determine the type of scrolling
        let isScrollUp = isScrollDirectionUp
        let isScrollingViaSectionIndices = scrollGestureIsActive
        recordContentOffset()
        
        // Find which current section that has been scrolled
        for section in 1..<tableView.numberOfSections {
            
            // Set the current section for the floating header based on the scroll position
            if tableView.rect(forSection: section).intersects(floatingHeaderRect) {
                currentSection = section
            }
            
            // If the section header intersects with the floating header, then we may need to adjust the headers
            let sectionHeaderRect = tableView.rectForHeader(inSection: section)
            if sectionHeaderRect.intersects(floatingHeaderRect) {
                
                var offsetInHeader = floatingHeaderRect.maxY - sectionHeaderRect.minY
                guard offsetInHeader < floatingHeaderRect.height else { continue }
                
                func updateFloatingHeaderIfNeeded() {
                    // When scrolling up, we need to preemptively refresh the floating header for the section that is being panned in
                    guard
                        isScrollUp || isScrollingViaSectionIndices,
                        _floatingHeaderSectionIndex != section-1
                        else { return }
                    
                    setup(header: floatingHeader, forSection: section-1, forceDivision: true)
                    _floatingHeaderSectionIndex = section-1
                    offsetInHeader += (floatingHeader.frame.height - floatingHeaderRect.height)
                }
                
                // When scrolling into a new division, this should always transition
                if isFirstSectionInDivision(section: section) {
                    updateFloatingHeaderIfNeeded()
                    floatingHeader.didScrollDivision(offsetInHeader: offsetInHeader)
                    didAdjustHeader = true
                    
                }
                    // When scrolling into a new section, only adjust header when didScrollIntoSection is still valid
                else if floatingHeader.didScrollIntoSection(offsetInHeader: offsetInHeader) {
                    updateFloatingHeaderIfNeeded()
                    didAdjustHeader = true
                }
            }
        }
    }
}

extension DivisionTableView {
    
    // MARK: - Convenience
    
    fileprivate var headerIdentifier: String {
        return String(describing: DivisionTableHeaderView.self)
    }
    
    // Update the last content offset for scroll direction
    fileprivate func recordContentOffset() {
        if tableView.contentOffset.y != _lastContentOffset {
            _lastContentOffset = tableView.contentOffset.y
        }
    }
    
    // Returns true if there is a current pan gesture or velocity from a preview gesture
    fileprivate var scrollGestureIsActive: Bool {
        let panGestureRecognizer = tableView.panGestureRecognizer
        return !(panGestureRecognizer.state == .possible && panGestureRecognizer.velocity(in: nil) == .zero)
    }
    
    /// Returns true if the user is scrolling the table up
    fileprivate var isScrollDirectionUp: Bool {
        if let lastContentOffset = _lastContentOffset {
            // Base this off the last location
            return lastContentOffset > tableView.contentOffset.y
        } else {
            // Base this off the panGestureTranslation (e.g. scrolling via velocity has no touch)
            return tableView.panGestureRecognizer.translation(in: self).y > 0
        }
    }
    
    // Converts the floating header frame into the tableview coordinate system
    fileprivate var rectForFloatingHeader: CGRect {
        return CGRect(origin: tableView.contentOffset, size: floatingHeader.frame.size)
    }
    
    
    fileprivate func setup(header: DivisionTableHeaderView?, forSection section: Int, forceDivision: Bool = false) {
        let (divisionIndex, sectionIndex) = mapSectionIndex(at: section)
        let showDivision = forceDivision || sectionIndex == 0
        let divisionView = showDivision ? dataSource?.divisionTableView(self.tableView, viewForHeaderInDivision: divisionIndex) : nil
        let sectionView = dataSource?.divisionTableView(self.tableView, viewForHeaderInSection: sectionIndex, division: divisionIndex)
        header?.setup(divisionView, sectionView)
    }
}

