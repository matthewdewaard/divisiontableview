import XCTest
@testable import DivisionTableViewTests

XCTMain([
    testCase(DivisionTableViewTests.allTests),
])
